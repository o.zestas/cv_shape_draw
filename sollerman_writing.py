import cv2
from HandTrackingModule import HandDetector
import time
import math
import numpy as np
from tkinter import *
import utils
import os
import csv

# CSV ##
output_csv = open('output/sollerman_writing_out.csv', 'a+')
headers = ['Time', 'Score', 'Hand', 'Shape_Name', 'Deviation', 'Draw_Radius']
writer = csv.DictWriter(output_csv, delimiter=',', lineterminator='\n', fieldnames=headers)
if output_csv.tell() == 0:
    writer.writeheader()
writeResults = True
########

# Drawing parameters
RED = (0, 0, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
PURPLE = (255, 0, 255)
THICKNESS = 3
FILLED = -1
deviation = []

SCORE_ONE_TWO_SEC = 60
SCORE_THREE_SEC = 40
SCORE_FOUR_SEC = 20

CLOSED_LENGTH_IT = 80  # distance between thumb and index
CLOSED_LENGTH_LT = 90  # distance between thumb and long

end_test_time = 0


def start(camera_id, file_name, hand, radius):
    DRAWING_POINT_RADIUS = STARTING_POINT_RADIUS = radius

    def endTest(score, mean_deviation):
        utils.putTextRect(dst, "Test Over", [300, 300], colorR=(35, 35, 129),
                          scale=6, thickness=5, offset=20)
        if score == 0 or score == 1:
            utils.putTextRect(dst, "Failed", [300, 450], colorR=(35, 35, 129),
                              scale=6, thickness=5, offset=20)
        else:
            utils.putTextRect(dst, "Success", [300, 450], colorR=(35, 35, 129),
                              scale=6, thickness=5, offset=20)
        utils.putTextRect(dst, f'Score: {score}', [300, 600], colorR=(35, 35, 129),
                          scale=6, thickness=5, offset=20)

        global writeResults, end_test_time
        if writeResults:
            end_test_time = time.time()
            writer.writerow({'Time': duration, 'Score': score, 'Hand': hand, 'Shape_Name': file_name,
                             'Deviation': mean_deviation, 'Draw_Radius': DRAWING_POINT_RADIUS})
            writeResults = False

        if int(time.time() - end_test_time) >= 5:
            exit()

    def check_collision(a, b, c, x, y, m_radius):
        # Finding the distance of line
        # from center.
        signed_dist = ((a * x + b * y + c) /
                       math.sqrt(a * a + b * b))
        deviation.append(signed_dist)
        unsigned_dist = abs(signed_dist)

        # Checking if the distance is less
        # than, greater than or equal to radius.
        if m_radius >= unsigned_dist:
            return True
        else:
            return False

    def prepare_target_image(image_path, scr_w, scr_h):
        target_image = cv2.imread(image_path)
        target_image = cv2.resize(target_image, (512, 512))

        # Create a blank image
        blank = np.zeros([scr_h, scr_w, 3], dtype=np.uint8)
        blank.fill(255)
        blank_height, blank_width, blank_c = blank.shape
        blank_cy, blank_cx = blank_height // 2, blank_width // 2

        # Place the target image in the center of the blank image
        blank[blank_cy - (512 // 2):blank_cy + (512 // 2), blank_cx - (512 // 2):blank_cx + (512 // 2)] = target_image

        return blank

    def preprocess_target_image(img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray, 50, 150, apertureSize=3)
        lines = cv2.HoughLinesP(image=edges, rho=1, theta=np.pi / 180, threshold=100, lines=np.array([]),
                                minLineLength=100, maxLineGap=80)
        all_points = dict([])
        final_points = dict()

        result = img.copy()
        result1 = img.copy()
        a, b, c = lines.shape
        all_points[('0', 'x1')] = []
        all_points[('0', 'y1')] = []
        all_points[('0', 'x2')] = []
        all_points[('0', 'y2')] = []
        all_points[('0', 'x1')].append(lines[0][0][0])
        all_points[('0', 'y1')].append(lines[0][0][1])
        all_points[('0', 'x2')].append(lines[0][0][2])
        all_points[('0', 'y2')].append(lines[0][0][3])
        p = 1
        for i in range(1, a):
            dup = False
            xi1, yi1, xi2, yi2 = lines[i][0]
            cv2.line(result1, (int(xi1), int(yi1)), (int(xi2), int(yi2)), (0, 0, 255), 2)
            slope = 0
            if xi2 - xi1 != 0:
                slope = ((yi2 - yi1) / (xi2 - xi1))
            print(f"line {i}")
            for j in range(0, p):
                xj1 = np.mean(all_points[f'{j}', 'x1'])
                yj1 = np.mean(all_points[f'{j}', 'y1'])
                xj2 = np.mean(all_points[f'{j}', 'x2'])
                yj2 = np.mean(all_points[f'{j}', 'y2'])
                d1 = distance(xi1, yi1, xj1, yj1)
                d2 = distance(xi2, yi2, xj2, yj2)
                cur_slope = 0
                if xj2 - xj1 != 0:
                    cur_slope = ((yj2 - yj1) / (xj2 - xj1))
                print(f'D1: {d1} | D2: {d2} | slope: {slope} | cur_slope: {cur_slope}')
                if (d1 <= 20 and d2 <= 20) or (
                        (d1 <= 20 or d2 <= 20) and slope != 0 and cur_slope != 0 and abs(slope - cur_slope) <= 0.02):
                    print("!!Duplicate Line!!")
                    all_points[(f'{j}', 'x1')].append(xi1)
                    all_points[(f'{j}', 'y1')].append(yi1)
                    all_points[(f'{j}', 'x2')].append(xi2)
                    all_points[(f'{j}', 'y2')].append(yi2)
                    dup = True
            print("--------")
            if not dup:
                all_points[(f'{p}', 'x1')] = []
                all_points[(f'{p}', 'y1')] = []
                all_points[(f'{p}', 'x2')] = []
                all_points[(f'{p}', 'y2')] = []
                all_points[(f'{p}', 'x1')].append(xi1)
                all_points[(f'{p}', 'y1')].append(yi1)
                all_points[(f'{p}', 'x2')].append(xi2)
                all_points[(f'{p}', 'y2')].append(yi2)
                p += 1

        for i in range(0, p):
            tx1 = np.mean(all_points[f'{i}', 'x1'])
            ty1 = np.mean(all_points[f'{i}', 'y1'])
            tx2 = np.mean(all_points[f'{i}', 'x2'])
            ty2 = np.mean(all_points[f'{i}', 'y2'])
            final_points[f'{i}', 'x1'] = tx1
            final_points[f'{i}', 'y1'] = ty1
            final_points[f'{i}', 'x2'] = tx2
            final_points[f'{i}', 'y2'] = ty2
            cv2.line(result, (int(tx1), int(ty1)), (int(tx2), int(ty2)), (0, 0, 255), 2)

        # cv2.imshow("recognized image", result)
        # cv2.imshow("original", result1)
        print(len(all_points))
        print(f'total lines: {p}')
        print(len(final_points))

        return final_points

    def distance(x1, y1, x2, y2):
        line_len = math.sqrt(((x2 - x1) ** 2 + (y2 - y1) ** 2))
        return line_len

    def is_between(ax, ay, cx, cy, bx, by):
        return abs(
            distance(ax, ay, cx, cy) + distance(cx, cy, bx, by) - distance(ax, ay, bx, by)) < DRAWING_POINT_RADIUS

    def find_start_end_point(lines):
        min_x = lines['0', 'x1']
        min_y = lines['0', 'y1']
        max_x = lines['0', 'x2']
        max_y = lines['0', 'y2']
        # if max_y < min_y and min_x - 5 <= max_x <= min_x + 5:
        #     max_y = min_y
        #     max_x = min_x
        min_line_id = 0
        for line in range(1, len(lines) // 4):
            tmp_min_x = lines[f'{line}', 'x1']
            tmp_min_y = lines[f'{line}', 'y1']
            tmp_max_x = lines[f'{line}', 'x2']
            tmp_max_y = lines[f'{line}', 'y2']
            print("------")
            print(f'minx: {min_x} miny: {min_y}')
            print(f'maxx: {max_x} maxy: {max_y}')
            print(f'x1: {tmp_min_x} y1: {tmp_min_y}')
            print(f'x2: {tmp_max_x} y2: {tmp_max_y}')
            # we have to do tmp > min because the point (0,0) is in the top-left corner (bottom-left point).
            if (tmp_min_y > min_y and tmp_min_x - 5 <= min_x <= tmp_min_x + 5) or tmp_min_x < min_x:
                min_x = tmp_min_x
                min_y = tmp_min_y
                min_line_id = line
            elif tmp_max_x < min_x:
                min_x = tmp_max_x
                min_y = tmp_min_y
                min_line_id = line
            # now to get the bottom-right point.
            if tmp_max_x > max_x:
                max_x = tmp_max_x
                max_y = tmp_max_y
            if tmp_max_y > max_y and tmp_min_x - 5 <= max_x <= tmp_min_x + 5:
                max_x = tmp_max_x
                max_y = tmp_max_y
            elif tmp_min_y > max_y and (tmp_max_x - 5 < max_x < tmp_max_x + 5 or max_x < tmp_max_x):
                max_x = tmp_max_x
                max_y = tmp_min_y

        # check for closed shapes (such as triangles, squares etc.), where endpoint = starting point
        for line in range(0, len(lines) // 4):
            if line == min_line_id:
                continue
            tmp_min_x = lines[f'{line}', 'x1']
            tmp_min_y = lines[f'{line}', 'y1']
            tmp_max_x = lines[f'{line}', 'x2']
            tmp_max_y = lines[f'{line}', 'y2']
            print("------")
            print(f'minx: {min_x} miny: {min_y}')
            print(f'x1: {tmp_min_x} y1: {tmp_min_y}')
            print(f'x2: {tmp_max_x} y2: {tmp_max_y}')
            if (tmp_max_x - 5 <= min_x <= tmp_max_x + 5) and (tmp_max_y - 5 <= min_y <= tmp_max_y + 5) \
                    or (tmp_min_x - 5 <= min_x <= tmp_min_x + 5) and (tmp_min_y - 5 <= min_y <= tmp_min_y + 5):
                max_x = min_x
                max_y = min_y
                break
        print("------")
        print(f'minx: {min_x} miny: {min_y}')
        print(f'maxx: {max_x} maxy: {max_y}')
        return min_x, min_y, max_x, max_y

    # Get screen size
    root = Tk()
    root.withdraw()
    screen_width, screen_height = root.winfo_screenwidth(), root.winfo_screenheight()
    # Image to overlay
    target_image_path = f'images/{file_name}'
    blank = prepare_target_image(image_path=target_image_path, scr_w=screen_width, scr_h=screen_height)

    # Preprocess the image to get the outline of the shape
    blank_copy = blank.copy()
    line_points = preprocess_target_image(blank_copy)
    line_params = dict()
    total_len = 0
    for l_id in range(0, len(line_points) // 4):
        xl1 = line_points[f'{l_id}', 'x1']
        yl1 = line_points[f'{l_id}', 'y1']
        xl2 = line_points[f'{l_id}', 'x2']
        yl2 = line_points[f'{l_id}', 'y2']
        # Total length of lines
        total_len += distance(xl1, yl1, xl2, yl2)
        # Line parameters (ax + by + c = 0)
        a_l = yl1 - yl2
        b_l = xl2 - xl1
        c_l = xl1 * yl2 - xl2 * yl1
        line_params[f'{l_id}', 'a'] = a_l
        line_params[f'{l_id}', 'b'] = b_l
        line_params[f'{l_id}', 'c'] = c_l

    startx, starty, endx, endy = find_start_end_point(line_points)
    # Starting point
    cv2.circle(blank, (int(startx), int(starty)), STARTING_POINT_RADIUS, RED, FILLED)
    # End point only if open shape
    if startx != endx and starty != endy:
        cv2.circle(blank, (int(endx), int(endy)), STARTING_POINT_RADIUS, BLUE, FILLED)

    ##############################################################################################################
    # Start video
    if os.name == 'nt':
        cap = cv2.VideoCapture(camera_id, cv2.CAP_DSHOW)
    else:
        cap = cv2.VideoCapture(camera_id, cv2.CAP_V4L2)
    cap.set(3, 1280)
    cap.set(4, 720)
    cap.set(cv2.CAP_PROP_FPS, int(60))
    detector = HandDetector(detectionCon=0.8, maxHands=1)

    # Booleans
    toStartGame = False
    toStartTime = False
    gameLost = False
    gameWon = False

    # Points and distance
    curr_pt_x, curr_pt_y = 0, 0
    prev_pts_x, prev_pts_y = [], []
    distance_drawn = 0

    # Time
    # pTime = 0 # for fps
    start_time = 0
    duration = 0

    while True:
        success, img = cap.read()

        # Adjust the size of the frame
        img = cv2.flip(img, 1)
        frame_height, frame_width, _ = img.shape
        scaleWidth = float(screen_width) / float(frame_width)
        scaleHeight = float(screen_height) / float(frame_height)

        # if scaleHeight > scaleWidth:
        #     imgScale = scaleWidth
        # else:
        #     imgScale = scaleHeight

        newX, newY = img.shape[1] * scaleWidth, img.shape[0] * scaleHeight
        img = cv2.resize(img, (int(newX), int(newY)))  # Screen width: 1440, Screen height: 1080

        hands, img = detector.findHands(img, flipType=False)

        if hands:
            lmList = hands[0]['lmList']
            length1, _ = detector.findDistance(lmList[4], lmList[8])
            length2, _ = detector.findDistance(lmList[4], lmList[12])

            if length1 < CLOSED_LENGTH_IT and length2 < CLOSED_LENGTH_LT:
                x1, y1, _ = lmList[8]

                curr_pt_x, curr_pt_y = x1, y1

                starting_pt_dist = distance(x2=curr_pt_x, x1=startx, y2=curr_pt_y, y1=starty)
                ending_pt_dist = distance(x2=curr_pt_x, x1=endx, y2=curr_pt_y, y1=endy)
                # Begin game
                if starting_pt_dist < 15:
                    toStartGame = True
                    toStartTime = True

                if toStartGame:
                    # Points drawn by player
                    prev_pts_x.append(curr_pt_x)
                    prev_pts_y.append(curr_pt_y)
                    cv2.circle(blank, (curr_pt_x, curr_pt_y), DRAWING_POINT_RADIUS, PURPLE, FILLED)

                    if len(prev_pts_x) > 2 and len(prev_pts_y) > 2:
                        # Calculate distance drawn
                        distance_drawn += distance(x2=curr_pt_x, x1=prev_pts_x[-2], y2=curr_pt_y, y1=prev_pts_y[-2])
                        # math.sqrt((curr_pt_x - prev_pts_x[-2]) ** 2 + (curr_pt_y - prev_pts_y[-2]) ** 2)

                    # print(f'Drawn: {distance_drawn} | total: {total_len}')
                    toLose = True
                    for l_id in range(0, len(line_points) // 4):
                        xl1 = line_points[f'{l_id}', 'x1']
                        yl1 = line_points[f'{l_id}', 'y1']
                        xl2 = line_points[f'{l_id}', 'x2']
                        yl2 = line_points[f'{l_id}', 'y2']
                        cv2.line(img, (int(xl1), int(yl1)), (int(xl2), int(yl2)), GREEN, THICKNESS)
                        # Check collision between index finger point and lines with two steps:
                        # First, check if the point is between the two points of the line.
                        is_point_between = is_between(xl1, yl1, x1, y1, xl2, yl2)
                        # Second, check for collision with the line.
                        # The following returns true if the finger is within region
                        # of the general line (even off-limits of the shape).
                        is_point_colliding = check_collision(line_params[f'{l_id}', 'a'], line_params[f'{l_id}', 'b'],
                                                             line_params[f'{l_id}', 'c'], int(x1), int(y1),
                                                             DRAWING_POINT_RADIUS)
                        if gameLost is False and is_point_between is True and is_point_colliding is True:
                            toLose = False

                    gameLost = toLose

                if toStartTime:
                    start_time = time.time()
                    toStartTime = False

                # End game
                if ending_pt_dist < 15 and distance_drawn > total_len:
                    toStartGame = False
                    gameWon = True

        if not gameLost:
            dst = cv2.addWeighted(img, 0.5, blank, 0.5, 0.0)
        else:
            dst = cv2.addWeighted(img, 0.5, blank_copy, 0.5, 0.0)

        utils.putTextRect(dst, "Sollerman Test - Writing", [50, 60], colorR=(35, 35, 129),
                          scale=3, thickness=3, offset=10)
        # Time taken
        if toStartGame is True and gameLost is False:
            end_time = time.time()
            duration = int(end_time - start_time)
            utils.putTextRect(dst, f'Time: {int(duration)} sec', [50, 100], colorR=(35, 35, 129),
                              scale=3, thickness=2, offset=10)

        if duration >= SCORE_ONE_TWO_SEC and distance_drawn >= 50:
            endTest(1, np.mean(deviation))
        elif duration >= SCORE_ONE_TWO_SEC:
            endTest(0, np.mean(deviation))
        elif gameWon and duration <= SCORE_FOUR_SEC:
            endTest(4, np.mean(deviation))
        elif gameWon and duration <= SCORE_THREE_SEC:
            endTest(3, np.mean(deviation))
        elif gameWon and duration <= SCORE_ONE_TWO_SEC:
            endTest(2, np.mean(deviation))
        elif gameLost:
            endTest(0, np.mean(deviation))
            # utils.putTextRect(dst, "Test Failed!", [50, 140], colorR=(35, 35, 129),
            #                  scale=3, thickness=3, offset=10)

        # FPS
        # cTime = time.time()
        # fps = 1 / (cTime - pTime)
        # pTime = cTime
        # cv2.putText(dst, f'FPS: {int(fps)}', (35, 220), cv2.FONT_HERSHEY_COMPLEX, 0.7, (255, 0, 0), 2)

        cv2.imshow("Sollerman Test - Writing", dst)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    print(f'Deviation: {np.mean(deviation)}')
    cap.release()
    cv2.destroyAllWindows()
    exit()
