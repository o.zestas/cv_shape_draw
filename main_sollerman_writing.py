from tkinter import *
from tkinter.ttk import Combobox
from tkinter.ttk import Separator

import utils
import sollerman_writing


def createUI():
    def start_sollerman_writing():
        camera_id = cb1.get()
        file_name = cb2.get()
        hand = cb3.get()
        radius = scale_radius.get()
        if camera_id == "No cameras available!" or file_name == "No JPG files found!":
            exit(1)
        else:
            window.destroy()
            sollerman_writing.start(int(camera_id[-1]), file_name, hand, radius)

    window = Tk()

    handeness = ("Left", "Right")
    cameraIndexes = utils.returnCameraIndexes()
    jpgFiles = sorted(utils.listJpgFilesInDirectory("images/"))

    if len(cameraIndexes) == 0:
        cameraIndexes.append("No cameras available!")
    if len(jpgFiles) == 0:
        jpgFiles.append("No JPG files found!")

    l_camera = Label(window, text="Camera")
    cb1 = Combobox(window, values=cameraIndexes, state="readonly", width=40)
    cb1.current(len(cameraIndexes) - 1)

    l_hand = Label(window, text="Hand")
    cb3 = Combobox(window, values=handeness, state="readonly", width=40)
    cb3.current(1)

    l_file = Label(window, text="File Name")
    cb2 = Combobox(window, values=jpgFiles, state="readonly", width=40)
    cb2.current(len(jpgFiles) - 1)

    scale_radius = Scale(window, from_=20, to=40, length=350, tickinterval=5, orient=HORIZONTAL)
    l_radius = Label(window, text="Drawing Point Radius")

    button_start = Button(window, height=2, width=20, text="Start!", command=lambda: start_sollerman_writing())
    separator1 = Separator(window, orient='horizontal')
    separator2 = Separator(window, orient='horizontal')
    separator3 = Separator(window, orient='horizontal')
    separator4 = Separator(window, orient='horizontal')

    separator1.pack(padx=10, pady=5, fill='x')
    l_camera.pack(pady=5)
    cb1.pack()

    separator3.pack(padx=10, pady=5, fill='x')
    l_hand.pack(pady=5)
    cb3.pack()

    l_file.pack(pady=5)
    cb2.pack()
    separator2.pack(padx=10, pady=10, fill='x')

    l_radius.pack(pady=5)
    scale_radius.set(20)
    scale_radius.pack()
    separator4.pack(padx=10, pady=10, fill='x')

    button_start.pack()
    window.title('Sollerman - Writing Options')
    window.geometry("420x420")
    window.resizable(False, False)
    window.mainloop()


if __name__ == "__main__":
    createUI()
