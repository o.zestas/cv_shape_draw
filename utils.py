import cv2
import os


def putTextRect(img, text, pos, scale=3, thickness=3, colorT=(255, 255, 255),
                colorR=(255, 0, 255), font=cv2.FONT_HERSHEY_PLAIN,
                offset=10, border=None, colorB=(0, 255, 0)):
    """
    Creates Text with Rectangle Background
    :param img: Image to put text rect on
    :param text: Text inside the rect
    :param pos: Starting position of the rect x1,y1
    :param scale: Scale of the text
    :param thickness: Thickness of the text
    :param colorT: Color of the Text
    :param colorR: Color of the Rectangle
    :param font: Font used. Must be cv2.FONT....
    :param offset: Clearance around the text
    :param border: Outline around the rect
    :param colorB: Color of the outline
    :return: image, rect (x1,y1,x2,y2)
    """
    ox, oy = pos
    (w, h), _ = cv2.getTextSize(text, font, scale, thickness)

    x1, y1, x2, y2 = ox - offset, oy + offset, ox + w + offset, oy - h - offset

    cv2.rectangle(img, (x1, y1), (x2, y2), colorR, cv2.FILLED)
    if border is not None:
        cv2.rectangle(img, (x1, y1), (x2, y2), colorB, border)
    cv2.putText(img, text, (ox, oy), font, scale, colorT, thickness)

    return img, [x1, y2, x2, y1]


def returnCameraIndexes():
    # checks the first 10 indexes.
    index = 0
    arr = []
    i = 10

    if os.name == 'nt':
        mode = cv2.CAP_DSHOW
    else:
        mode = cv2.CAP_V4L2

    while i > 0:
        cap = cv2.VideoCapture(index, mode)
        if cap.read()[0]:
            arr.append("Camera ID: " + str(index))
            cap.release()
        index += 1
        i -= 1
    return arr


def listJpgFilesInDirectory(directory):
    files = os.listdir(directory)
    # Filtering only jpg files and no folders
    files = [f for f in files if os.path.isfile(directory + '/' + f) and f.endswith(".jpg")]

    return files
